// Fill out your copyright notice in the Description page of Project Settings.


#include "PGameManager.h"

// Sets default values
APGameManager::APGameManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APGameManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APGameManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

