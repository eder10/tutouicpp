// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "tutouicppGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TUTOUICPP_API AtutouicppGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
