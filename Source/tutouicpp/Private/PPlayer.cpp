// Fill out your copyright notice in the Description page of Project Settings.


#include "PPlayer.h"
#include "Components/MeshComponent.h"

// Sets default values
APPlayer::APPlayer()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;
}

// Called when the game starts or when spawned
void APPlayer::BeginPlay()
{
	Super::BeginPlay();
	
}

void APPlayer::MoveRight(float Value)
{
	//Logic from tutorial
	AddMovementInput(GetActorRightVector(), MaxSpeed * Value, false);
}

// Called every frame
void APPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Logic from Tutorial
	FVector InputVector= ConsumeMovementInputVector();
	InputVector *= DeltaTime;
	AddActorLocalOffset(InputVector, true, nullptr);

}

// Called to bind functionality to input
void APPlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAxis("MoveRight", this, &APPlayer::MoveRight);

}

